package factory

import (
	"api/handler"
	"encoding/json"
)

func Test(dataByte []byte) (isBreak bool) {
	type receivedData struct {
		Action string `json:"action"`
	}
	var data receivedData
	if err := json.Unmarshal(dataByte, &data); err == nil {
		switch data.Action {
		case "Test":
			handler.Test(dataByte)
		}
	}
	return false
}

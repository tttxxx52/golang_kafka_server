package handler

import (
	"api/util/kafka"
	"api/util/log"
	"encoding/json"
)

func Test(dataByte []byte) (isBreak bool) {
	type receivedData struct {
		Uuid   string `json:"uuid"`
	}

	var data receivedData
	var status = true
	var message = "Response message"

	if err := json.Unmarshal(dataByte, &data); err != nil {
		message = "Error"
	}


	jsonMap := map[string]interface{}{
		"uuid":    data.Uuid,
		"status":  status,
		"message": message,
	}
	if _, err := kafka.Push("Response", jsonMap); err != nil {
		log.Error(err)
	}
	return false
}



//+build test

package main

import (
	"github.com/gin-gonic/gin"
	"trade/config"
)

func init() {
	gin.SetMode(gin.TestMode)
	config.InitConfig()
}
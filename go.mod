module api

go 1.13

require (
	github.com/Shopify/sarama v1.24.1
	github.com/gin-gonic/gin v1.5.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/cpuid v1.2.2 // indirect
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	google.golang.org/appengine v1.6.5 // indirect
)

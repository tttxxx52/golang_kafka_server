//+build !test

package main

import (
	"github.com/gin-gonic/gin"
	"api/config"
)

func init() {
	gin.SetMode(gin.ReleaseMode)
	config.InitConfig()
}
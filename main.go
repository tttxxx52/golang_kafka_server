package main

import (
	"api/config"
	"api/factory"
	"api/handler"
	"api/util/kafka"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	"runtime"
)

func init() {
	_ = godotenv.Load(".env")
	gin.SetMode(gin.DebugMode)
	config.InitConfig()
}

func main() {
	//direct handler
	go kafka.Listen("Request", handler.Test)

	//have router
	go kafka.Listen("reqUNCWallet", factory.Test)

	runtime.Goexit()
}


